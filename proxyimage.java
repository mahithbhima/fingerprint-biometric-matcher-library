import java.util.*;

public class proxyimage {
	private double value;
	public double getValue() {
		return value;
	}
	
	public double getQuality(proxyimage I) {
		//Some logic
		double v=I.getValue();
		
		if(v<0 || v>10)return -1;
		else {
			return v/100;
		}
		
	}
	
	public double getMatch(proxyimage I1,proxyimage I2) {
		double v1=I1.getValue();
		double v2=I2.getValue();
		
		//Some logic
		double dif=Math.abs(v1-v2);
		
		return ((10-dif)/10)*100;
		
	}
	
	public ArrayList<Double> getMatchN(proxyimage I, ArrayList<proxyimage> A) {
		
		int N=A.size();
		ArrayList<Double> S=new ArrayList<Double>(N);
		for(int i=0;i<N;i++) {
			S.set(i, getMatch(I,A.get(i)));
		}
		
		return S;
	}
	
	
	
}

